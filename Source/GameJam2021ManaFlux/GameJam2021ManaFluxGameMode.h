// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameJam2021ManaFluxGameMode.generated.h"

UCLASS(minimalapi)
class AGameJam2021ManaFluxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGameJam2021ManaFluxGameMode();
};



