// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameJam2021ManaFluxGameMode.h"
#include "GameJam2021ManaFluxCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGameJam2021ManaFluxGameMode::AGameJam2021ManaFluxGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
